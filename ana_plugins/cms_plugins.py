from django.utils.translation import ugettext as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from . import models


@plugin_pool.register_plugin
class EventAndLocationFinder(CMSPluginBase):
    model = models.EventAndLocationFinder
    render_template = 'ana_plugins/event_and_location_finder.html'
    name = _('Event And Location Finder')

    def get_form(self, request, obj=None, **kwargs):
        form = super(EventAndLocationFinder, self).get_form(request, obj, **kwargs)

        if obj is None:
            plugin_language = request.GET.get('plugin_language', None)
            preselected_language = self.get_preselected_language(plugin_language)
            form.base_fields['display_language'].initial = preselected_language

        return form

    def get_preselected_language(self, plugin_language):
        for code, desc in self.model.LANGUAGES:
            if code == plugin_language:
                return code
        for code, desc in self.model.LANGUAGES:
            if plugin_language.startswith(code):
                return code
        return self.model.LANGUAGES[0][0]
