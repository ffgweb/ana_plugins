# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-12-14 08:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ana_plugins', '0003_eventandlocationfinder_display_language'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventandlocationfinder',
            name='start_page',
            field=models.CharField(choices=[(b'/events', b'Events'), (b'/stores', b'Stores')], default=b'/events', max_length=32, verbose_name='Start page'),
        ),
    ]
