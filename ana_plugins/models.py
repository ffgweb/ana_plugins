import json

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext as _
from cms.models.pluginmodel import CMSPlugin


class EventAndLocationFinder(CMSPlugin):

    ROUTER_HASH = 'hash'
    ROUTER_PATH = 'path'

    PAGE_EVENTS = '/events'
    PAGE_STORES = '/stores'

    LANGUAGES = (
        ('en', _('English')),
        ('de', _('German')),
        ('es', _('Spanish')),
        ('fr', _('French')),
        ('it', _('Italian')),
    )

    widget_url = models.URLField(
        default='https://s3.us-east-2.amazonaws.com/locator.asmodee.com/locations-widget.js',
        blank=True,
        verbose_name=_('Widget URL'),
        help_text=_('Fallbacks to settings.EVENT_AND_LOCATION_FINDER_WIDGET_URL when blank'),
    )

    studios = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=_('Studios'),
        help_text=_('Comma separated codes or empty for all'),
    )

    product_line = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=_('Product Line ID'),
        help_text=_('ID or empty for all'),
    )

    router_type = models.CharField(
        max_length=32,
        default=ROUTER_HASH,
        choices=(
            (ROUTER_HASH, ROUTER_HASH,),
            (ROUTER_PATH, ROUTER_PATH,),
        ),
        verbose_name=_('Router type'),
    )

    start_page = models.CharField(
        max_length=32,
        default=PAGE_EVENTS,
        choices=(
            (PAGE_EVENTS, 'Events',),
            (PAGE_STORES, 'Stores',),
        ),
        verbose_name=_('Start page'),
    )

    display_language = models.CharField(
        max_length=2,
        choices=LANGUAGES,
        verbose_name=_('Display language'),
    )

    @property
    def html_div_id(self):
        return 'map-widget-root-{}'.format(self.pk)

    @property
    def studio_list(self):
        return [s.strip() for s in self.studios.split(',') if s.strip()]

    @property
    def params_json(self):
        return json.dumps({
            'root': self.html_div_id,
            'hashRouter': self.router_type == self.ROUTER_HASH,
            'studios': self.studio_list,
            'productLine': self.product_line if self.product_line else None,
            'language': self.display_language,
            'startPage': self.start_page,
        })

    @property
    def proper_widget_url(self):
        if self.widget_url:
            return self.widget_url

        return getattr(settings, 'EVENT_AND_LOCATION_FINDER_WIDGET_URL', '')
