import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'VERSION')) as version_file:
    version = version_file.read().strip()

setup(
    name='ana_plugins',
    version=version,
    author='Asmodee NA',
    packages=[
        'ana_plugins',
        'ana_plugins.migrations',
    ],
    include_package_data=True,
    install_requires=[
        'django-cms',
    ]
)
